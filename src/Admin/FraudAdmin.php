<?php
namespace App\Admin;

use App\Entity\Questions;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class FraudAdmin extends AbstractAdmin
{
    public function toString(object $object): string
    {
        return $object instanceof Questions
            ? 'Fraud'
            : 'Fraud'; // shown in the breadcrumb on the create view
    }

    protected function configureFormFields(FormMapper $form): void
    {
        $form->add('word', TextareaType::class);


    }

    protected function configureDatagridFilters(DatagridMapper $datagrid): void
    {
        $datagrid->add('id');
        $datagrid->add('word');




    }

    protected function configureListFields(ListMapper $list): void
    {

        $list->addIdentifier('word');



        $list->add(ListMapper::NAME_ACTIONS, null, [
            'actions' => [
                'show' => [
                    'template' => 'sonata/Crud/list_action_show.html.twig',
                ],
                'edit' => [
                    'template' => 'sonata/Crud/list_action_edit.html.twig',
                ],
                'delete' => [
                    'template' => 'sonata/Crud/list_action_delete.html.twig',
                ],
            ]
        ]);
    }

    protected function configureShowFields(ShowMapper $show): void
    {
        $show->add('word');


    }

}
