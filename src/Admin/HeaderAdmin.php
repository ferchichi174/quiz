<?php
// src/Admin/HeaderAdmin.php

namespace App\Admin;

use App\Entity\Header;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Vich\UploaderBundle\Form\Type\VichImageType;

final class HeaderAdmin extends AbstractAdmin
{
    public function toString(object $object): string
    {
        return $object instanceof Header
            ? 'Header'
            : 'Header'; // shown in the breadcrumb on the create view
    }

    protected function configureFormFields(FormMapper $form): void
    {
        $form->add('Heading', TextareaType::class);
        $form->add('description', TextareaType::class);
        $form->add('quiz');
        $form->add('imageFile', VichImageType::class, [
            'required' => false,
            'allow_delete' => true, // not mandatory, default is true
            'download_uri' => true, // not mandatory, default is true
        ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagrid): void
    {
        $datagrid->add('Heading');
        $datagrid->add('description');
    }

    protected function configureListFields(ListMapper $list): void
    {
        $list->addIdentifier('Heading');
        $list->addIdentifier('description');
        $list->addIdentifier('quiz');

//        $list->add('image', null, [
//            'template' => 'admin/list_image.html.twig'
//        ]);

        $list->add(ListMapper::NAME_ACTIONS, null, [
            'actions' => [
                'show' => [
                    'template' => 'sonata/Crud/list_action_show.html.twig',
                ],
                'edit' => [
                    'template' => 'sonata/Crud/list_action_edit.html.twig',
                ],
                'delete' => [
                    'template' => 'sonata/Crud/list_action_delete.html.twig',
                ],
            ]
        ]);
    }

    protected function configureShowFields(ShowMapper $show): void
    {
        $show->add('Heading');
        $show->add('description');
        $show->add('image', null, [
            'template' => 'admin/show_image.html.twig'
        ]);
    }
}
