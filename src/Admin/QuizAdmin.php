<?php
namespace App\Admin;

use App\Entity\Quiz;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\HttpFoundation\RequestStack;

final class QuizAdmin extends AbstractAdmin
{
    private RequestStack $requestStack;

    public function __construct($code, $class, $baseControllerName, RequestStack $requestStack)
    {
        parent::__construct($code, $class, $baseControllerName);
        $this->requestStack = $requestStack;
    }

    public function toString(object $object): string
    {
        return $object instanceof Quiz ? 'Quiz' : 'Quiz'; // shown in the breadcrumb on the create view
    }

    protected function configureFormFields(FormMapper $form): void
    {
        $form->add('name');
    }

    protected function configureDatagridFilters(DatagridMapper $datagrid): void
    {
        $datagrid->add('id');
        $datagrid->add('name');
        $datagrid->add('url');
        $datagrid->add('revenu');
    }

    protected function configureListFields(ListMapper $list): void
    {
        $list->addIdentifier('name');
        $list->addIdentifier('url');
        $list->addIdentifier('views');
        $list->addIdentifier('clicks');
        $list->addIdentifier('revenu');


        $list->add(ListMapper::NAME_ACTIONS, null, [
            'actions' => [
                'show' => ['template' => 'sonata/Crud/list_action_show.html.twig'],
                'edit' => ['template' => 'sonata/Crud/list_action_edit.html.twig'],
                'delete' => ['template' => 'sonata/Crud/list_action_delete.html.twig'],
            ]
        ]);
    }

    protected function configureShowFields(ShowMapper $show): void
    {
        $show->add('name');
        $show->add('url');
        $show->add('question', null, [
            'associated_property' => 'question'
        ]);

    }

    public function prePersist(object $quiz): void
    {
        if ($quiz instanceof Quiz) {
            $quiz->setUrl($this->createQuizUrl($quiz->getName()));
        }
    }

    private function createQuizUrl(string $name): string
    {
        $request = $this->requestStack->getCurrentRequest();
        if (!$request) {
            throw new \RuntimeException('Cannot determine the current request.');
        }

        $hostUrl = $request->getSchemeAndHttpHost() . '/';
        $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $name)));
        return $hostUrl . $slug;
    }
}
