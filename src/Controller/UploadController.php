<?php

namespace App\Controller;

use App\Entity\Header;
use App\Entity\Questions;
use App\Entity\Answers;
use App\Repository\QuizRepository;
use App\Repository\TemplateRepository;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UploadController extends AbstractController
{
    #[Route('/admin/upload', name: 'app_upload')]
    public function index(QuizRepository $quizRepository, TemplateRepository $templateRepository): Response
    {
        $quiz = $quizRepository->findAll();
        $temp = $templateRepository->findAll();

        // Assuming you need images related to templates or any specific logic to fetch images
        $images = array_map(fn($t) => $t->getImage(), $temp); // or any other logic

        return $this->render('upload/upload.html.twig', [
            'quiz' => $quiz,
            'temp' => $temp,
            'images' => $images,
        ]);
    }


    #[Route('/admin/upload/process', name: 'app_upload_process', methods: ['POST'])]
    public function process(Request $request, QuizRepository $quizRepository, EntityManagerInterface $entityManager,TemplateRepository $templateRepository): Response
    {
        $file = $request->files->get('csv_files');
        $quizId = $request->request->get('csv_category');
        $temimg = $request->request->get('csv_images');

        // Retrieve the selected quiz
        $selectedQuiz = $quizRepository->find($quizId);
        $selectedtemp = $templateRepository->findOneBy(['image'=> $temimg]);


        if (!$selectedQuiz) {
            $this->addFlash('error', 'Invalid quiz selected.');
            return $this->redirectToRoute('app_upload');
        }
        $selectedQuiz->setTemplate($selectedtemp);
        $entityManager->persist($selectedQuiz);

        if ($file && $file->isValid()) {
            $spreadsheet = IOFactory::load($file->getPathname());
            $worksheet = $spreadsheet->getActiveSheet();

            $currentQuestion = null;
            $rank = 1;

            foreach ($worksheet->getRowIterator() as $rowIndex => $row) {
                $cells = $row->getCellIterator();
                $cells->setIterateOnlyExistingCells(false); // Loop through all cells, even if they are not set
                $data = [];
                foreach ($cells as $cell) {
                    $data[] = $cell->getValue();
                }

                if (!empty($data[0])) {
                    $cellValue = trim($data[0]);

                    // Vérifier et insérer les données de Header
                    if (strtolower($cellValue) === 'heading') {
                        $heading = isset($data[1]) ? trim($data[1]) : null;
                        $description = isset($data[2]) ? trim($data[2]) : null;

                        if (!empty($heading)) {
                            $header = new Header();
                            $header->setHeading($heading);
                            $header->setDescription($description);
                            $header->setQuiz($selectedQuiz);
                            $entityManager->persist($header);
                        }
                        continue; // Skip to the next row after processing Header
                    }

                    // Check for question pattern
                    if (preg_match('/^Question \d+:/i', $cellValue)) {
                        if ($currentQuestion) {
                            $entityManager->persist($currentQuestion);
                        }
                        // Create a new question
                        $currentQuestion = new Questions();
                        $currentQuestion->setQuestion(trim(substr($cellValue, strpos($cellValue, ':') + 1)));
                        $currentQuestion->setRank($rank++);
                        $currentQuestion->setQuiz($selectedQuiz); // Associate the question with the selected quiz
                    } elseif ($currentQuestion && !empty($cellValue)) {
                        // Add answers to the current question
                        $answer = new Answers();
                        $answer->setAnswer($cellValue);
                        $answer->setQuestion($currentQuestion);
                        $entityManager->persist($answer);
                    }
                }
            }

            // Persist the last question if it exists
            if ($currentQuestion) {
                $entityManager->persist($currentQuestion);
            }

            $entityManager->flush();

            $this->addFlash('success', 'Headers, questions, and answers have been imported successfully.');
        } else {
            $this->addFlash('error', 'Invalid file or no file uploaded.');
        }

        return $this->redirectToRoute('app_upload');
    }

}
