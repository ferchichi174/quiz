<?php

namespace App\Controller;

use App\Entity\Exquiz;
use App\Entity\Utcsource;
use App\Repository\ExquizRepository;
use App\Repository\FraudRepository;
use App\Repository\QuizRepository;
use Doctrine\ORM\EntityManagerInterface;
use Google\Analytics\Data\V1beta\BetaAnalyticsDataClient;
use Google\Analytics\Data\V1beta\DateRange;
use Google\Analytics\Data\V1beta\Dimension;
use Google\Analytics\Data\V1beta\Metric;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Google\Analytics\Data\V1beta\Filter;
use Google\Analytics\Data\V1beta\FilterExpression;
use Google\Analytics\Data\V1beta\Filter\StringFilter;
use Google\Analytics\Data\V1beta\Filter\StringFilter\MatchType;
class FraudController extends AbstractController
{

    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    #[Route('/api/fraud', name: 'app_fraud')]
    public function index(FraudRepository $fraudRepository): Response
    {
        $frauds = $fraudRepository->findAll();

        return $this->json([
            'frauds' => $frauds,
        ]);
    }

    #[Route('/admin/google', name: 'app_google')]
    public function google(QuizRepository $quizRepository,EntityManagerInterface $entityManager): Response
    {
        $credentialsPath = $_SERVER['DOCUMENT_ROOT'] . 'quizdashboard-423812-112feba3a6cc.json';

        // Check if the credentials file exists
        if (!file_exists($credentialsPath)) {
            throw new \Exception('Google application credentials file not found: ' . $credentialsPath);
        }

        // Set the environment variable for Google Application Credentials
        putenv('GOOGLE_APPLICATION_CREDENTIALS=' . $credentialsPath);

        // Property ID for Google Analytics
        $property_id = '440185763';

        try {
            // Instantiate the Analytics Data API client
            $client = new BetaAnalyticsDataClient();

            // Prepare the API request
            $response = $client->runReport([
                'property' => 'properties/' . $property_id,
                'dateRanges' => [
                    new DateRange([
                        'start_date' => '2020-03-31',
                        'end_date' => 'today',
                    ]),
                ],
                'dimensions' => [
                    new Dimension([
                        'name' => 'unifiedPagePathScreen',
                    ]),
                ],
                'metrics' => [
                    new Metric([
                        'name' => 'totalAdRevenue',
                    ]),
                    new Metric([
                        'name' => 'publisherAdImpressions',
                    ]),
                    new Metric([
                        'name' => 'publisherAdClicks',
                    ]),
                    new Metric([
                        'name' => 'screenPageViews',
                    ]),
                ],
            ]);

            $links_result = $quizRepository->findAll();
            $urls = [];
            foreach ($links_result as $row){
                $urls[] = $row->getName();
            }
            $revenue = 0;
            $ad_clicks = 0;
            // Output the response
            foreach ($response->getRows() as $row) {
                $url_path = ltrim($row->getDimensionValues()[0]->getValue(), '/');
                if (in_array($url_path, $urls)) {
                    $revenue = $row->getMetricValues()[0]->getValue();
                    $ad_impressions = $row->getMetricValues()[1]->getValue();
                    $ad_clicks = $row->getMetricValues()[2]->getValue();
                    $views = $row->getMetricValues()[3]->getValue();


               $quiz =  $quizRepository->findOneBy(['name' => $url_path]);
               $formattedRevenue = number_format($revenue, 3);
                $quiz->setRevenu($formattedRevenue);
                $quiz->setViews($views);
                $quiz->setClicks($ad_clicks);
                $entityManager->persist($quiz);
                }


            }
            $entityManager->flush();



        } catch (\Exception $e) {


            // Handle exceptions
            $this->addFlash('error', 'Error running the Google Analytics report: ' . $e->getMessage());
            return $this->redirectToRoute('admin_app_quiz_list');
        }
        $this->addFlash('success', 'Google Analytics data has been successfully updated.');
        return $this->redirectToRoute('admin_app_quiz_list');
    }



    #[Route] #[Route('/admin/extrquiz', name: 'app_extr_quiz')]
    public function quiz(ExquizRepository $quizRepository,Request $request): Response
    {
        $utmSource = $request->query->get('utm_source');

        $Exquiz = $quizRepository->findAll();
        foreach ($Exquiz as $ex){


            $this->Analytic($ex,$utmSource);
        }


        return $this->redirectToRoute('admin_app_exquiz_list');
    }

    #[Route] #[Route('/admin/extrquizsource', name: 'app_extr_source_quiz')]
    public function source(ExquizRepository $quizRepository,Request $request): Response
    {


        $Exquiz = $quizRepository->findAll();
        foreach ($Exquiz as $ex){


            $this->Analytic2($ex);
        }


        return $this->redirectToRoute('admin_app_exquiz_list');
    }



    public function Analytic($Exquiz,$utmSource): Response
    {
        $credentialsPath = $_SERVER['DOCUMENT_ROOT'] . 'quizdashboard-423812-112feba3a6cc.json';

        if (!file_exists($credentialsPath)) {
            throw new \Exception('Google application credentials file not found: ' . $credentialsPath);
        }

        putenv('GOOGLE_APPLICATION_CREDENTIALS=' . $credentialsPath);

        $property_id = $Exquiz->urlid;

        try {
            $client = new BetaAnalyticsDataClient();

            $response = $client->runReport([
                'property' => 'properties/' . $property_id,
                'dateRanges' => [
                    new DateRange([
                        'start_date' => '2020-03-31',
                        'end_date' => 'today',
                    ]),
                ],
                'dimensions' => [
                    new Dimension([
                        'name' => 'unifiedPagePathScreen',
                    ]),
                    new Dimension([
                        'name' => 'sessionSource',
                    ]),
                ],
                'metrics' => [
                    new Metric([
                        'name' => 'totalAdRevenue',
                    ]),
                    new Metric([
                        'name' => 'publisherAdImpressions',
                    ]),
                    new Metric([
                        'name' => 'publisherAdClicks',
                    ]),
                    new Metric([
                        'name' => 'screenPageViews',
                    ]),
                ],
                'dimensionFilter' => new FilterExpression([
                    'filter' => new Filter([
                        'field_name' => 'sessionSource',
                        'string_filter' => new StringFilter([
                            'match_type' => MatchType::EXACT,
                            'value' => $utmSource
                        ]),
                    ]),
                ]),
            ]);

            $revenue = 0;
            $ad_clicks = 0;
            $path = $Exquiz->url;
            if (!preg_match("~^(?:f|ht)tps?://~i", $path)) {
                $path = "http://" . $path;
            }
            $parsedUrl = parse_url($path, PHP_URL_PATH);
            $lastSegment = $parsedUrl ? end(explode('/', trim($parsedUrl, '/'))) : "";

            foreach ($response->getRows() as $row) {
                $url_path = ltrim($row->getDimensionValues()[0]->getValue(), '/');
                $utm_source = $row->getDimensionValues()[1]->getValue();
                    $utm_sources[] = $utm_source;

                if ($url_path == $lastSegment) {
                    $revenue = $row->getMetricValues()[0]->getValue();
                    $ad_impressions = $row->getMetricValues()[1]->getValue();
                    $ad_clicks = $row->getMetricValues()[2]->getValue();
                    $views = $row->getMetricValues()[3]->getValue();

                    $Exquiz->setRevenu($revenue);
                    $Exquiz->setClicks($ad_clicks);
                    $Exquiz->setViews($views);
                    $Exquiz->setUtmSource($utm_source);

                    $this->entityManager->persist($Exquiz);
                }
            }
            $utcsourceRepository = $this->getDoctrine()->getRepository(Utcsource::class);

            foreach($utm_sources as $utm){

                $existingUtc = $utcsourceRepository->findOneBy(['name' => $utm]);
                if (!$existingUtc) {
                    $utc = new Utcsource();
                    $utc->setName($utm);
                    $this->entityManager->persist($utc);
                }

            }

            $this->entityManager->flush();

        } catch (\Exception $e) {
            $this->addFlash('error', 'Error running the Google Analytics report: ' . $e->getMessage());
            return $this->redirectToRoute('admin_app_exquiz_list');
        }

        $this->addFlash('success', 'Google Analytics data has been successfully updated.');
        return $this->redirectToRoute('admin_app_exquiz_list');
    }

    public function Analytic2($Exquiz): Response
    {
        $credentialsPath = $_SERVER['DOCUMENT_ROOT'] . 'quizdashboard-423812-112feba3a6cc.json';

        if (!file_exists($credentialsPath)) {
            throw new \Exception('Google application credentials file not found: ' . $credentialsPath);
        }

        putenv('GOOGLE_APPLICATION_CREDENTIALS=' . $credentialsPath);

        $property_id = $Exquiz->urlid;

        try {
            $client = new BetaAnalyticsDataClient();

            $response = $client->runReport([
                'property' => 'properties/' . $property_id,
                'dateRanges' => [
                    new DateRange([
                        'start_date' => '2020-03-31',
                        'end_date' => 'today',
                    ]),
                ],
                'dimensions' => [
                    new Dimension([
                        'name' => 'unifiedPagePathScreen',
                    ]),
                    new Dimension([
                        'name' => 'sessionSource',
                    ]),
                ],
                'metrics' => [
                    new Metric([
                        'name' => 'totalAdRevenue',
                    ]),
                    new Metric([
                        'name' => 'publisherAdImpressions',
                    ]),
                    new Metric([
                        'name' => 'publisherAdClicks',
                    ]),
                    new Metric([
                        'name' => 'screenPageViews',
                    ]),
                ],

            ]);

            $revenue = 0;
            $ad_clicks = 0;
            $path = $Exquiz->url;
            if (!preg_match("~^(?:f|ht)tps?://~i", $path)) {
                $path = "http://" . $path;
            }
            $parsedUrl = parse_url($path, PHP_URL_PATH);
            $lastSegment = $parsedUrl ? end(explode('/', trim($parsedUrl, '/'))) : "";

            foreach ($response->getRows() as $row) {
                $url_path = ltrim($row->getDimensionValues()[0]->getValue(), '/');
                $utm_source = $row->getDimensionValues()[1]->getValue();
                $utm_sources[] = $utm_source;

            }
            $utcsourceRepository = $this->getDoctrine()->getRepository(Utcsource::class);

            foreach($utm_sources as $utm){

                $existingUtc = $utcsourceRepository->findOneBy(['name' => $utm]);
                if (!$existingUtc) {
                    $utc = new Utcsource();
                    $utc->setName($utm);
                    $this->entityManager->persist($utc);
                }

            }

            $this->entityManager->flush();

        } catch (\Exception $e) {
            $this->addFlash('error', 'Error running the Google Analytics report: ' . $e->getMessage());
            return $this->redirectToRoute('admin_app_exquiz_list');
        }

        $this->addFlash('success', 'Utm_source data has been successfully updated.');
        return $this->redirectToRoute('admin_app_exquiz_list');
    }

    #[Route] #[Route('/autocomplete/utm_source', name: 'autocomplete_utm_source')]
    public function autocompleteUtcsource(Request $request, EntityManagerInterface $entityManager)
    {
        $term = $request->query->get('term');
        $utcsourceRepository = $entityManager->getRepository(Utcsource::class);

        $results = $utcsourceRepository->createQueryBuilder('u')
            ->where('u.name LIKE :term')
            ->setParameter('term', '%' . $term . '%')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();

        $suggestions = [];
        foreach ($results as $result) {
            $suggestions[] = $result->getName();
        }

        return new JsonResponse($suggestions);
    }
}