<?php

namespace App\Controller;

use App\Entity\Quiz;
use App\Repository\HeaderRepository;
use App\Repository\QuestionsRepository;
use App\Repository\QuizRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class QuizController extends AbstractController
{
    #[Route('/{name}', name: 'app_quiz')]
    public function show(Quiz $urlQuiz,QuestionsRepository $questionsRepository,HeaderRepository $headerRepository): Response
    {


        $questions = $questionsRepository->findAllWithquiz($urlQuiz->getName());
        $firstQuestionId = $questions[0]->getId();


        $header = $headerRepository->findOneByQuizId($urlQuiz->getId());
        if ($urlQuiz->getName() == "fitai" ){

            return $this->render('questions/landing-three.html.twig', [
                'questions' => $questions,
                'header' =>$header,
                'firstQuestion' =>$firstQuestionId,
                'name' =>$urlQuiz->getName()
            ]);
        }

        if ($urlQuiz->getTemplate() && $urlQuiz->getTemplate()->getName() == "theme3" ){

            return $this->render('questions/landing-two.html.twig', [
                'questions' => $questions,
                'header' =>$header,
                'firstQuestion' =>$firstQuestionId,
                'name' =>$urlQuiz->getName()
            ]);
        }

        if ( $urlQuiz->getName() == "fitaien"){

            return $this->render('questions/landing-two.html.twig', [
                'questions' => $questions,
                'header' =>$header,
                'firstQuestion' =>$firstQuestionId,
                'name' =>$urlQuiz->getName()
            ]);
        }
        return $this->render('questions/lading.html.twig', [
            'questions' => $questions,
            'header' =>$header,
            'firstQuestion' =>$firstQuestionId,
            'name' =>$urlQuiz->getName()
        ]);
    }

    #[Route('/{name}/{questionId}', name: 'app_quiz_question', requirements: ['questionId' => '\d+'])]
    public function showQuestion(string $name, int $questionId, QuestionsRepository $questionsRepository, HeaderRepository $headerRepository): Response
    {
        $question = $questionsRepository->find($questionId);
        $quiz = $question->getQuiz();
        $questions = $questionsRepository->findBy(['quiz' => $quiz]);

        // Get the total number of questions and the next question ID
        $totalQuestions = count($questions);
        $nextQuestionId = null;

        foreach ($questions as $index => $q) {
            if ($q->getId() === $questionId && isset($questions[$index + 1])) {
                $nextQuestionId = $questions[$index + 1]->getId();
                break;
            }
        }

        $header = $headerRepository->findOneByQuizId($quiz->getId());
        $firstQuestionId = $questions[0]->getId();

        if ($quiz->getName() == "fitai" || $quiz->getName() == "fitaien"){
            return $this->render('questions/index-two.html.twig', [
                'question' => $question,
                'header' => $header,
                'totalQuestions' => $totalQuestions,
                'nextQuestionId' => $nextQuestionId,
                'isFirstQuestion' => $questionId === $firstQuestionId,
                'name' =>$quiz
            ]);
        }

        if ($quiz->getTemplate()->getName() == "theme3"){
            return $this->render('questions/index-two.html.twig', [
                'question' => $question,
                'header' => $header,
                'totalQuestions' => $totalQuestions,
                'nextQuestionId' => $nextQuestionId,
                'isFirstQuestion' => $questionId === $firstQuestionId,
                'name' =>$quiz
            ]);
        }
        return $this->render('questions/index.html.twig', [
            'question' => $question,
            'header' => $header,
            'totalQuestions' => $totalQuestions,
            'nextQuestionId' => $nextQuestionId,
            'isFirstQuestion' => $questionId === $firstQuestionId,
            'name' =>$quiz
        ]);
    }


        #[Route('/{name}/result', name: 'app_result')]
    public function index($name): Response
    {
        if ($name == "fitai"){
            return $this->render('result/index-two.html.twig', [

            ]);

        }
        return $this->render('result/index.html.twig', [

        ]);


    }
}
