<?php

namespace App\Controller;

use App\Repository\HeaderRepository;
use App\Repository\QuestionsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class QuestionsController extends AbstractController
{
    #[Route('/', name: 'app_questions')]
    public function index(QuestionsRepository $questionsRepository,HeaderRepository $headerRepository): Response
    {
        $questions = $questionsRepository->findAllWithAnswers();
        $header = $headerRepository->createQueryBuilder('h')
            ->setFirstResult(0)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();


        return $this->render('questions/index.html.twig', [
            'questions' => $questions,
            'header' =>$header
            ]);
    }
}
