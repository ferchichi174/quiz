<?php

namespace App\Entity;

use App\Repository\QuizRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: QuizRepository::class)]
class Quiz
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    private ?string $url = null;

    #[ORM\OneToMany(targetEntity: Questions::class, mappedBy: 'quiz',orphanRemoval: true, cascade: ['persist', 'remove'])]
    private Collection $question;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $revenu = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $views = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $clicks = null;

    #[ORM\ManyToOne(inversedBy: 'quiz')]
    private ?Template $template = null;

    public function __construct()
    {
        $this->question = new ArrayCollection();
    }

    public function __toString()
    {
        return ($this->name);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): static
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return Collection<int, Questions>
     */
    public function getQuestion(): Collection
    {
        return $this->question;
    }

    public function addQuestion(Questions $question): static
    {
        if (!$this->question->contains($question)) {
            $this->question->add($question);
            $question->setQuiz($this);
        }

        return $this;
    }

    public function removeQuestion(Questions $question): static
    {
        if ($this->question->removeElement($question)) {
            // set the owning side to null (unless already changed)
            if ($question->getQuiz() === $this) {
                $question->setQuiz(null);
            }
        }

        return $this;
    }

    public function getRevenu(): ?string
    {
        return $this->revenu;
    }

    public function setRevenu(?string $revenu): static
    {
        $this->revenu = $revenu;

        return $this;
    }

    public function getViews(): ?string
    {
        return $this->views;
    }

    public function setViews(?string $views): static
    {
        $this->views = $views;

        return $this;
    }

    public function getClicks(): ?string
    {
        return $this->clicks;
    }

    public function setClicks(?string $clicks): static
    {
        $this->clicks = $clicks;

        return $this;
    }

    public function getTemplate(): ?Template
    {
        return $this->template;
    }

    public function setTemplate(?Template $template): static
    {
        $this->template = $template;

        return $this;
    }
}
