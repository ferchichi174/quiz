<?php

namespace App\Entity;

use App\Repository\ExquizRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ExquizRepository::class)]
class Exquiz
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    public ?string $url = null;

    #[ORM\Column(length: 255)]
    public ?string $urlid = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $views = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $clicks = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $revenu = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $utm_source = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): static
    {
        $this->url = $url;

        return $this;
    }

    public function getUrlid(): ?string
    {
        return $this->urlid;
    }

    public function setUrlid(string $urlid): static
    {
        $this->urlid = $urlid;

        return $this;
    }

    public function getViews(): ?string
    {
        return $this->views;
    }

    public function setViews(?string $views): static
    {
        $this->views = $views;

        return $this;
    }

    public function getClicks(): ?string
    {
        return $this->clicks;
    }

    public function setClicks(?string $clicks): static
    {
        $this->clicks = $clicks;

        return $this;
    }

    public function getRevenu(): ?string
    {
        return $this->revenu;
    }

    public function setRevenu(?string $revenu): static
    {
        $this->revenu = $revenu;

        return $this;
    }

    public function getUtmSource(): ?string
    {
        return $this->utm_source;
    }

    public function setUtmSource(?string $utm_source): static
    {
        $this->utm_source = $utm_source;

        return $this;
    }
}
