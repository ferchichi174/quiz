<?php

namespace App\Block;



use App\Repository\ExquizRepository;
use App\Repository\QuizRepository;
use App\Repository\SendRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Block\Service\AbstractBlockService;
use Sonata\BlockBundle\Block\Service\BlockServiceInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Twig\Environment;

class DashblockService extends AbstractBlockService implements BlockServiceInterface
{
    /**
     * @var SendRepository
     */

    private $quizRepository;
    private $extquizRepository;

    private $twig;

    public function __construct(Environment $twig,QuizRepository $quizRepository ,ExquizRepository $extquizRepository)
    {
        $this->twig = $twig;
        $this->quizRepository = $quizRepository;
        $this->extquizRepository = $extquizRepository;

    }



    public function configureSettings(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([

            'title' => 'title',
            'template' => 'sonata/admin/dashblock.html.twig',
            'extraVariable' => "extraVariable",
        ]);
    }


    public function execute(BlockContextInterface $blockContext, ?Response $response = null): Response
    {


        $settings = $blockContext->getSettings();

        $content = $this->twig->render($blockContext->getTemplate(), [
            'title' => $settings['title'],
            'exquiz' => $this->extquizRepository->count(),
            'quiz' => $this->quizRepository->count(),
            'settings' => $settings,

        ]);

        if (null === $response) {
            $response = new Response();
        }

        $response->setContent($content);

        return $response;
    }

}
