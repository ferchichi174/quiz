<?php

namespace App\Block;



use App\Repository\QuizRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Block\Service\AbstractBlockService;
use Sonata\BlockBundle\Block\Service\BlockServiceInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Twig\Environment;

class DashBlockCourbeService extends AbstractBlockService implements BlockServiceInterface
{
    private $twig;
    private $quizRepository;
    public function __construct(Environment $twig,QuizRepository $quizRepository)
    {
        $this->twig = $twig;
        $this->quizRepository = $quizRepository;
    }



    public function configureSettings(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([

            'title' => 'qsdqs',
            'template' => 'sonata/admin/dashblockcourbe.html.twig',
            'extraVariable' => "sdfsd",
        ]);
    }


    public function execute(BlockContextInterface $blockContext, ?Response $response = null): Response
    {
        $settings = $blockContext->getSettings();

        $content = $this->twig->render($blockContext->getTemplate(), [
            'title' => $settings['title'],
            'quiz' => $this->quizRepository->findBy([], null, 4),
            'settings' => $settings,
        ]);

        if (null === $response) {
            $response = new Response();
        }

        $response->setContent($content);

        return $response;
    }



}
