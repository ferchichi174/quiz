<?php

namespace App\Repository;

use App\Entity\Exquiz;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Exquiz>
 *
 * @method Exquiz|null find($id, $lockMode = null, $lockVersion = null)
 * @method Exquiz|null findOneBy(array $criteria, array $orderBy = null)
 * @method Exquiz[]    findAll()
 * @method Exquiz[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExquizRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Exquiz::class);
    }

//    /**
//     * @return Exquiz[] Returns an array of Exquiz objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('e.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Exquiz
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
