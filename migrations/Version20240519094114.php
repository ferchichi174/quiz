<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240519094114 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE header ADD quiz_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE header ADD CONSTRAINT FK_6E72A8C1853CD175 FOREIGN KEY (quiz_id) REFERENCES quiz (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6E72A8C1853CD175 ON header (quiz_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE header DROP FOREIGN KEY FK_6E72A8C1853CD175');
        $this->addSql('DROP INDEX UNIQ_6E72A8C1853CD175 ON header');
        $this->addSql('ALTER TABLE header DROP quiz_id');
    }
}
