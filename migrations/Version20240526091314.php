<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240526091314 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE exquiz (id INT AUTO_INCREMENT NOT NULL, url VARCHAR(255) NOT NULL, urlid VARCHAR(255) NOT NULL, views VARCHAR(255) DEFAULT NULL, clicks VARCHAR(255) DEFAULT NULL, revenu VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE header DROP FOREIGN KEY FK_6E72A8C1853CD175');
        $this->addSql('ALTER TABLE header ADD CONSTRAINT FK_6E72A8C1853CD175 FOREIGN KEY (quiz_id) REFERENCES quiz (id)');
        $this->addSql('ALTER TABLE quiz DROP FOREIGN KEY FK_A412FA925DA0FB8');
        $this->addSql('ALTER TABLE quiz ADD CONSTRAINT FK_A412FA925DA0FB8 FOREIGN KEY (template_id) REFERENCES template (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE exquiz');
        $this->addSql('ALTER TABLE header DROP FOREIGN KEY FK_6E72A8C1853CD175');
        $this->addSql('ALTER TABLE header ADD CONSTRAINT FK_6E72A8C1853CD175 FOREIGN KEY (quiz_id) REFERENCES quiz (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE quiz DROP FOREIGN KEY FK_A412FA925DA0FB8');
        $this->addSql('ALTER TABLE quiz ADD CONSTRAINT FK_A412FA925DA0FB8 FOREIGN KEY (template_id) REFERENCES template (id) ON DELETE CASCADE');
    }
}
